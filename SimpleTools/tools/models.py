from django.db import models
import datetime
class TimeReg(models.Model):
    dato = models.DateField(default=datetime.datetime.now())
    sagsnummer = models.CharField(max_length=200)
    beskrivelse=models.TextField(max_length=1000)
    timer_brugt = models.FloatField(default=0)
    ijira = models.BooleanField(default=False)
    itimereg = models.BooleanField(default=False)

    def __str__(self):
        return self.sagsnummer

