# Generated by Django 2.1.2 on 2018-11-17 22:55

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TimeReg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dato', models.DateField(default=datetime.datetime(2018, 11, 17, 23, 55, 6, 344206))),
                ('sagsnummer', models.CharField(max_length=200)),
                ('beskrivelse', models.TextField(max_length=1000)),
                ('ijira', models.BooleanField(default=False)),
                ('itimereg', models.BooleanField(default=False)),
            ],
        ),
    ]
