from django.shortcuts import render

from django.http import HttpResponse
from django.views import generic
from tools.models import TimeReg


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def showSagTimer(request):
    sag = request.GET.get('sag','')
    saglist = TimeReg.objects.filter(sagsnummer=sag).order_by('-dato')
    navn = ''
    if(saglist.count()>0):
        navn = saglist.first().sagsnummer

    return render(request, 'timeregs/sagtimer_list.html',{'all':saglist,'navn':navn})



class TimeRegListView(generic.ListView):
    model = TimeReg
    context_object_name = 'mine_timer'
    queryset = TimeReg.objects.all().order_by("-dato")
    template_name ='timereg_list.html'





class SagListView(generic.ListView):
    model = TimeReg
    context_object_name = 'sag_timer'
    template_name ='sagtimer_list.html'

    def get(self,**kwargs):
        self.sag = kwargs['sag']
        print (self.sag)
        context = super(SagListView, self).get(**kwargs)
        context[self.context_object_name] = TimeReg.objects.filter(sagsnummer=self.sag).order_by('-dato')
        return context



